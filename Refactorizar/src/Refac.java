* Clase para refactorizar
 * Codificacion UTF-8
 * Paquete refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea de de bloque.
 */

import java.util.Scanner;
// No se debe incluir todas las clases en un import, hay que meter cada una en una linea diferente
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
// La clase no puede empezar con minúscula
public class Refac {
	final static String BIENVENIDA = "Bienvenido al programa";
	public static void main(String[] args) {
		//Es recomendable declarar las variables con el nombre de la variable especifica y una por linea
		Scanner c = new Scanner(System.in);
		
		System.out.println(BIENVENIDA);
		System.out.println("Introduce tu dni");
		String dni = c.nextLine();
		System.out.println("Introduce tu nombre");
		String nombre = c.nextLine();
		
		//cada declaracion en una linea
		int numeroA=7; 
		int numeroB=16;
		int numeroC = 25;
		//separar las variables cuando vayan a utilizarse y separando los operadores por parentesis
		if( (numeroA > numeroB) || (numeroC%5) != 0 && (numeroC*3)-1 > (numeroB/numeroC))
		{
			System.out.println("Se cumple la condición");
		}
		
		numeroC = numeroA+(numeroB*numeroC)+(numeroB/numeroA);
		//Los corchetes van pegados al tipo de datos
		String[] diasSemana = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
		//En los metodos no hay barras bajas y la A seria con mayuscula
		visualizarDiasSemana(diasSemana);
	}
	//Los corchetes van pegados al tipo de datos
	static void visualizarDiasSemana(String[] diasSemana)
	{
		//el indice de un bucle comienza con "i"
		for(int i=0 ;i < diasSemana.length ;i++) 
		{
			System.out.println("El dia de la semana en el que te encuentras"
					+ " ["+(i+1)+"-7] es el dia: "+diasSemana[i]);
		}//dar espacios para que se vea mas claro
	}
	
}