/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

//Nuna poner * y especificar cual es en concreto
//import java.io.* no es necesario porque no se utiliza
import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactor {

	static Scanner a;
	public static void main(String[] args) 
	{
	
		a = new Scanner(System.in);
		//cada declaracion de datos en una linea
		//especificar en los tipos de datos y no poner "n" sino "numero"
		int numero; 
		int cantidadMaximaAlumnos;
		
		cantidadMaximaAlumnos = 10;
		//Los corchetes van pegados al tipo de datos
		int[] arrays = new int[10];
		for(numero=0;numero<10;numero++)
		{
			System.out.println("Introduce nota media de alumno");
			arrays[numero] = a.nextInt();
			//Al ser un numero es nextInt(), no nextLine()
		}	
		//En un metodo no se ponen barras bajas y se ponen may�suclas
		System.out.println("El resultado es: " + media(arrays));
		
		//Faltan los parentesis para cerrar el scanner
		a.close();
	}
	//En un metodo no se ponen barras bajas y se ponen may�suclas
	//Los corchetes van pegados al tipo de datos
	//En vez de "recorrerArray" ponerlo mas concreto, y en este caso poner "media"
	static double media(int[] vector)
	{
		////especificar en los tipos de datos y no poner "c", sino algo mas concreto, y lo mismo con la "a"
		double c = 0;
		for(int a=0;a<10;a++) 
		{
			c=c+vector[a];
		}
		return c/10;
	}
	
}